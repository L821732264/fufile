# FuFile
FuFile is a high-performance, fault-tolerant, and scalable large-scale distributed storage system that provides storage and query of multiple data structures and files.
# Links
[Web Site](https://fufile.org) (under construction)
