# FuFile
![License](https://fufile-architecture.oss-cn-beijing.aliyuncs.com/apache2.0.svg)
### 简介
FuFile是一个高性能、容错和可扩容的大规模分布式存储系统，提供多种数据类型结构和文件的存储和查询。
### 链接
[官方网站](https://fufile.org)（施工中）
### 架构

### 功能特点
+ 自研高性能rpc
+ 存储节点可伸缩
+ 持续的服务监控
+ 服务高可用，灾难冗余，自动恢复
+ 支持巨大或小文件存储
+ 支持大规模的文件存储
+ 机架感知
+ 根据存储容量负载均衡存储服务
### 模块
+ client：客户端
+ core：核心功能
+ data-server：存储服务
+ name-server：名字服务
+ example：例子
+ generator: 代码生成
### 贡献
如果你想参与贡献，请发邮件到xuffcc@gmail.com。



